//The contact information is stored within a variable
var support = {
    "email": "studentwellbeing@edgehill.ac.uk",
    "tel": "01695 650988"
}
//The variable is to be outputted into the element in the HTML with the corresponding id
var output = document.getElementById('support');
//The text inputted into the HTML identified with the id is then set, using the identifiers used to store the information in the variable
output.innerHTML = "<li><strong>@: </strong>" + support.email + "</li>" + "<li><strong>Tel: </strong>" + support.tel + "</li>";
//The same is then done for the links displayed on the page
var links = {
    "moneyguide": "resources/Periodic-Table-of-Savings-Booklet-Digital-v2.pdf",
    "livingoncampus": "https://www.edgehill.ac.uk/study/accommodation/living-on-campus/",
    "visit": "https://blogs.edgehill.ac.uk/insideedge/2017/07/11/places-near-ormskirk-to-visit/",
    "studentblog": "https://blogs.edgehill.ac.uk/insideedge/"
}

var output = document.getElementById('money');
output.innerHTML = "<li><a href=" +  links.moneyguide + ">" + "A Guide To Saving" + "</a></li>";

var output = document.getElementById('lifestyle');
output.innerHTML = "<li><a href=" +  links.livingoncampus + ">" + "Living On Campus" + "</a></li>";

var output = document.getElementById('visit');
output.innerHTML = "<li><a href=" +  links.visit + ">" + "Students Suggest" + "</a></li>";

var output = document.getElementById('experiences');
output.innerHTML = "<li><a href=" +  links.studentblog + ">" + "Student Blog" + "</a></li>";
