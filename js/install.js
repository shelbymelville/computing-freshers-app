window.addEventListener('beforeinstallprompt', saveBeforeInstallPromptEvent);
deferredInstallPrompt = evt;
installButton.removeAttribute('hidden');

//Show install prompt & hide the install button.
deferredInstallPrompt.prompt();
// Hide the install button, it can't be called twice.
evt.srcElement.setAttribute('hidden', true);

// User response to prompt logged
deferredInstallPrompt.userChoice
    .then((choice) => {
      if (choice.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt', choice);
      } else {
        console.log('User dismissed the A2HS prompt', choice);
      }
      deferredInstallPrompt = null;
    });

    //Event listener added for appinstalled event
    window.addEventListener('appinstalled', logAppInstalled);

    //Code added to log event
    console.log('Freshers App was installed.', evt);
