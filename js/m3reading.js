//The reading lists is stored within a variable
var dataJSON = '[{"Title":"Information architecture for the World Wide Web (2006)","Author":"Morville, Peter; Rosenfeld, Louis"},{"Title":"Web design: a complete introduction","Author":"Chapman, Nigel P.; Chapman, Jenny (2006)"},{"Title":"Searching & researching on the Internet and the World Wide Web (2010)","Author":"Hartman, Karen; Ackermann, Ernest C.; Hartman, Karen "},{"Title":"Web style guide: basic design principles for creating Web sites (2008)","Author":"Lynch, Patrick J.; Horton, Sarah"},{"Title":"Dont make me think!: a common sense approach to Web usability (2006)","Author":"Krug, Steve"},{"Title":"Dont make me think, revisited: a common sense approach to web usability (2014)","Author":"Krug, Steve"},{"Title":"CSS Web site design (2007)","Author":"Meyer, Eric A."},{"Title":"HTML, XHTML & CSS (2007)","Author":"Castro, Elizabeth; Castro, Elizabeth"},{"Title":"Prioritizing Web usability (2006)","Author":"Nielsen, Jakob; Loranger, Hoa"},{"Title":"Speaking in styles: fundamentals of CSS for Web designers (2008)","Author":"Teague, Jason Cranford"},{"Title":"Learning Web design: a beginners guide to HTML, CSS, JavaScript, and web graphics (2012)","Author":"Niederst Robbins, Jennifer"},{"Title":"Visual design for the modern Web (2008)","Author":"McIntire, Penny"},{"Title":"Head first Web design (2009)","Author":"Watrall, Ethan; Siarto, Jeff"},{"Title":"Head first HTML and CSS (2012)","Author":"Robson, Elisabeth; Freeman, Eric; Robson, Elisabeth"},{"Title":"Head first JavaScript (2007)","Author":"Morrison, Michael"},{"Title":"CSS: the definitive guide (2006)","Author":"Meyer, Eric A.; Meyer, Eric A."}]';
//The variable is then parsed
var dataObject = JSON.parse(dataJSON);
//Creating a list item
var listItemString = $('#m3listItem').html();
//A new list is built for every item of the list (book)
dataObject.forEach(buildNewList);
//The list is then built
function buildNewList(item, index) {
  //The list item is stored between tags
  var listItem = $('<li>' + listItemString + '</li>');
  //The id where the list items are stored is identified
  var listItemTitle = $('.m3booktitle', listItem);
  //As well as the item in the list that should be stored within that id area
  listItemTitle.html(item.Title);
  //This is repeated for the author
  var listItemAuth = $('.m3author', listItem);
  listItemAuth.html(item.Author);
  $('#m3dataList').append(listItem);
}
