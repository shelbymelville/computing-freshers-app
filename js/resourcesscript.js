// The title, description and assessment type are stored within a variable for each module that is called later on
var m1 = {
    "title": "CIS1107 FOUNDATIONS OF COMPUTER SCIENCE",
    "content": "Foundations of Computer Science introduces you to the concepts and practices of problem solving through a series of innovative class seminars and individual experiments.  You will learn to identify a problem, analyse it, and produce a structured solution. Exploring common issues within computing, you will also see how all of the separate areas merge, overlap and depend on each other in order for a robust computing system to be developed.",
    "assessment": "Coursework: 100%."
}
//The variable is to be outputted into the element in the HTML with the corresponding id
var output = document.getElementById('m1title');
var output = document.getElementById('m1title2');
//The text inputted into the HTML identified with the id is then set, using the identifiers used to store the information in the variable
output.innerHTML = m1.title;
//This process is then repeated for the module content
var output = document.getElementById('m1content');
output.innerHTML = m1.content;
//And the assessment type
var output = document.getElementById('m1assessment');
output.innerHTML = "Assessment:" + " " + m1.assessment;

//This is then repeated for the further modules using the same method as above
var m2 = {
    "title": "CIS1108 DIGITAL WORLD: INFORMATION SYSTEMS AND DESIGN",
    "content": "Digital World: Information Systems and Design explores how the world of the 21st century is underpinned by computing technology. The module will develop your understanding of the essential concepts of systems analysis, including identifying systems processes, understanding business needs and presenting appropriate IT solutions. You will then apply this knowledge of systems analysis and design methods to make suggestions for improvement of a business, its processes and information systems.",
    "assessment": "Coursework: 80%, Practical(s): 20%."
}

var output = document.getElementById('m2title');
output.innerHTML = m2.title;

var output = document.getElementById('m2content');
output.innerHTML = m2.content;

var output = document.getElementById('m2assessment');
output.innerHTML = "Assessment:" + " " + m2.assessment;

var m5 = {
    "title": "CIS1109 DIGITAL WORLD: COMPUTER ARCHITECHTURE AND NETWORKS",
    "content": "Digital World: Computer Architecture and Networks explores how the world of the 21st century is underpinned by computing technology while challenging you to envision your future developments as a Computing professional. The module discusses how current and modern computer architectures operates. It also analyses the technology on which computer architecture depends, starting with current central processing units (CPUs) and their instruction sets and progressing to the principles of modern multitasking operating systems that are supported by the underlying hardware architectures. An important aspect of modern computer technology is networking. The module introduces and covers basic principles of networks, their interconnecting components and protocols used in enabling reliable communications.",
    "assessment": "Coursework: 70%, Written Exam(s): 30%."
}

var output = document.getElementById('m5title');
output.innerHTML = m5.title;

var output = document.getElementById('m5content');
output.innerHTML = m5.content;

var output = document.getElementById('m5assessment');
output.innerHTML = "Assessment:" + " " + m5.assessment;

var m3 = {
    "title": "CIS1110 WEB DESIGN AND DEVELOPMENT",
    "content": "Web Design and Development is a fusion of two distinct areas in the world of web content production.The module will enable you to develop a sound understanding of the World Wide Web, the related technologies, the relationships between them and also their use. You will also explore colour and design theories, layout and typography. In addition, the module provides an appropriate setting to introduce some of the more powerful, user friendly web development tools used widely in the industry today, along with HTML and CSS and both client and server side scripting.",
    "assessment": "Coursework: 100%."
}

var output = document.getElementById('m3title');
output.innerHTML = m3.title;

var output = document.getElementById('m3content');
output.innerHTML = m3.content;

var output = document.getElementById('m3assessment');
output.innerHTML = "Assessment:" + " " + m3.assessment;

var m4 = {
    "title": "CIS1111 PROGRAMMING: CONCEPTS TO CONSTRUCTION 1",
    "content": "Programming: Concepts to Construction 1 provides a practical introduction to the fundamentals of an object-oriented approach to software development. You will be introduced to the analytical techniques and processes that are essential for specifying, designing and implementing applications.",
    "assessment": "Coursework: 100%."
}

var output = document.getElementById('m4title');
var output = document.getElementById('m4title2');
output.innerHTML = m4.title;

var output = document.getElementById('m4content');
output.innerHTML = m4.content;

var output = document.getElementById('m4assessment');
output.innerHTML = "Assessment:" + " " + m4.assessment;

var m6 = {
    "title": "CIS1112 PROGRAMMING: CONCEPTS TO CONSTRUCTION 2",
    "content": "Programming: Concepts to Construction 2 provides a practical introduction to the fundamentals of an object-oriented approach to software development. You will explore and develop object-oriented modelling techniques and receive an introduction to programming through event-driven program design and graphical user interfaces.",
    "assessment": "Coursework: 100%."
}

var output = document.getElementById('m6title');
output.innerHTML = m6.title;

var output = document.getElementById('m6content');
output.innerHTML = m6.content;

var output = document.getElementById('m6assessment');
output.innerHTML = "Assessment:" + " " + m6.assessment;
