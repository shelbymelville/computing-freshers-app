// The times and names of societies are stored within a variable that is called later on
var societies = {
    "monsocietytime": "6PM",
    "monsocietyname": "Harry Potter Society",
    "monsocietyvenue": "(Hub1)",
    "tuesocietytime": "6PM",
    "tuesocietyname": "Video Game Society",
    "tuesocietyvenue": "(Hub 2)",
    "wedsocietytime": "5PM",
    "wedsocietyname": "Women In Society",
    "wedsocietyvenue": "B101",
    "thusocietytime": "6PM",
    "thusocietyname": "Anime Society",
    "thusocietyvenue": "(Hub 2)",
    "frisocietytime": "6PM",
    "frisocietyname": "Video Game Society",
    "frisocietyvenue": "(Hub 2)"
}

//The variable is to be outputted into the element in the HTML with the corresponding id
//Monday
var output = document.getElementById('mon-society');
//The text inputted into the HTML identified with the id is then set, using the identifiers used to store the information in the variable
output.innerHTML = "<td>" + societies.monsocietytime + "</td>" + "<td>" + societies.monsocietyname + "</td>" + "<td>" + societies.monsocietyvenue + "</td>";
//This is done for each day of the week to display all the societies in the variable
//Tuesday
var output = document.getElementById('tue-society');
output.innerHTML = "<td>" + societies.tuesocietytime + "</td>" + "<td>" + societies.tuesocietyname + "</td>" + "<td>" + societies.tuesocietyvenue + "</td>";
//Wednesday
var output = document.getElementById('wed-society');
output.innerHTML = "<td>" + societies.wedsocietytime + "</td>" + "<td>" + societies.wedsocietyname + "</td>" + "<td>" + societies.wedsocietyvenue + "</td>";
//Thursday
var output = document.getElementById('thu-society');
output.innerHTML = "<td>" + societies.thusocietytime + "</td>" + "<td>" + societies.thusocietyname + "</td>" + "<td>" + societies.thusocietyvenue + "</td>";
//Friday
var output = document.getElementById('fri-society');
output.innerHTML = "<td>" + societies.frisocietytime + "</td>" + "<td>" + societies.frisocietyname + "</td>" + "<td>" + societies.frisocietyvenue + "</td>";
