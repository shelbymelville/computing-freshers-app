// Attempted method
// var dataJSON = '[{"Facility":"MCCOLLS","OpeningHours":"Mon-Sun (6AM-11PM)"},{"Facility":"SAGES","OpeningHours":"Mon-Fri (7:30AM-9PM) Sat-Sun (10:30AM-9PM)"}, {"Facility":"STARBUCKS","OpeningHours":"Mon-Fri (7:30AM-6:30PM) Sat-Sun (1PM-5PM)"}]';
//
// var dataObject = JSON.parse(dataJSON);
// var listItemString = $('#listItem').html();
//
// dataObject.forEach(buildNewList);
//
// function buildNewList(item, index) {
//  var listItem = $('<li>' + listItemString + '</li>');
//  var listItemTitle = $('.title', listItem);
//  listItemTitle.html(item.Facility);
//
//  var listItemDesc = $('.description', listItem);
//  listItemDesc.html(item.OpeningHours);
//  $('#dataList').append(listItem);
// }

// The opening times for the facilities are stored within a variable for each module that is called later on
var opening = {
    "mccollsopening": "Mon-Sun (6AM-11PM)",
    "sagesopening": "Mon-Fri (7:30AM-9PM) <br> Sat-Sun (10:30AM-9PM)",
    "gngopening": "Mon-Fri (9AM-5PM)",
    "starbucksopening": "Mon-Fri (7:30AM-6:30PM) <br> Sat-Sun(1PM-5PM)",

    "sportsopening": "Mon-Thu (7AM-10PM) <br> Fri (7AM-9PM) <br> Sat-Sun (9AM-5PM)",
    "poolopening": "Mon-Thu (7AM-9PM) <br> Fri (7AM-8PM) <br> Sat-Sun (9AM-4PM) <br> (Closed 9AM-11AM Tue & Thu)",
    "fitnessopening": "Mon-Thu (7AM-10PM) <br> Fri (7AM-9PM) <br> Sat-Sun (9AM-5PM)",

    "helpdeskopening": "Mon-Fri (8AM-8PM) <br> Sat-Sun (10AM-6PM)",
    "degreesopening": "Mon-Fri (7AM-10PM)",

    "redbaropening": "Mon-Fri (10AM-6PM)",

    "wedgeopening": "Mon-Fri (8AM-3:45PM)",

    "rewindopening": "Mon-Fri (7:45AM-2:15PM)"
}
//The variable is to be outputted into the element in the HTML with the corresponding id
var output = document.getElementById('mccollsopening');
//The text inputted into the HTML identified with the id is then set, using the identifiers used to store the information in the variable
output.innerHTML = "<li>" + opening.mccollsopening; + "</li>";
//This process is then repeated for the other facilities using the same method
var output = document.getElementById('sagesopening');
output.innerHTML = "<li>" + opening.sagesopening + "</li>";
var output = document.getElementById('gngopening');
output.innerHTML = "<li>" + opening.gngopening + "</li>";
var output = document.getElementById('starbucksopening');
output.innerHTML = "<li>" + opening.starbucksopening; + "</li>";

var output = document.getElementById('sportsopening');
output.innerHTML = "<li>" + opening.sportsopening; + "</li>";
var output = document.getElementById('poolopening');
output.innerHTML = "<li>" + opening.poolopening + "</li>";
var output = document.getElementById('fitnessopening');
output.innerHTML = "<li>" + opening.fitnessopening + "</li>";

var output = document.getElementById('helpdeskopening');
output.innerHTML = "<li>" + opening.helpdeskopening; + "</li>";
var output = document.getElementById('degreesopening');
output.innerHTML = "<li>" + opening.degreesopening + "</li>";

var output = document.getElementById('redbaropening');
output.innerHTML = "<li>" + opening.redbaropening + "</li>";

var output = document.getElementById('wedgeopening');
output.innerHTML = "<li>" + opening.wedgeopening + "</li>";

var output = document.getElementById('rewindopening');
output.innerHTML = "<li>" + opening.rewindopening + "</li>";
