//The materials for the specific modules are stored in a variable
var  m2materials= {
    "referencing": "resources/Havard%2520Referencing%25202014%2520v2.4.pdf"
}
//The variable is to be outputted into the element in the HTML with the corresponding id
var output = document.getElementById('m2materials');
//The text inputted into the HTML identified with the id is then set, using the identifiers used to store the information in the variable
output.innerHTML = "<li><a href=" +  m2materials.referencing + ">" + "Harvard Referencing" + "</a></li>";
//This process is then repeated for the materials for the other modules
var  m3materials= {
    "html": "https://www.w3schools.com/html/default.asp",
    "js": "https://www.w3schools.com/js/",
    "css": "https://www.w3schools.com/css/default.asp",
    "cssgame": "https://cssgridgarden.com/",
    "flexboxgame": "https://flexboxfroggy.com/"
}

var output = document.getElementById('m3materials');
output.innerHTML = "<li><a href=" +  m3materials.html + ">" + "Introduction to HTML5" + "</a></li>" + "<li><a href=" +  m3materials.js + ">" + "Introduction to JavaScript" + "</a></li>" + "<li><a href=" +  m3materials.css + ">" + "Introduction to CSS" + "</a></li>" + "<li><a href=" +  m3materials.cssgame + ">" + "CSS Grid Game" + "</a></li>" + "<li><a href=" +  m3materials.flexboxgame + ">" + "Flexbox Game" + "</a></li>" ;

var  m4materials= {
    "alice": "https://www.alice.org/resources/alice-3-how-tos/",
    "java": "https://www.w3schools.com/java/"
}

var output = document.getElementById('m4materials');
output.innerHTML = "<li><a href=" +  m4materials.alice + ">" + "How to use Alice" + "</a></li>" + "<li><a href=" +  m4materials.java + ">" + "Introduction to Java" + "</a></li>";
