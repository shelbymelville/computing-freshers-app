//The reading lists is stored within a variable
var dataJSON = '[{"Title":"Beginning programming with Java for dummies (2014)","Author":"Burd, Barry A."},{"Title":"Java for dummies (2014)","Author":"Burd, Barry A."},{"Title":"Sams teach yourself Java in 21 days (2016)","Author":"Cadenhead, Rogers"},{"Title":"Big Java: early objects (2014)","Author":"Horstmann, Cay S."},{"Title":"Java in easy steps (2014)","Author":"McGrath, Mike"},{"Title":"Head first Java (2005)","Author":"Sierra, Kathy; Bates, Bert"},{"Title":"Alice 3 in action: computing through animation (2014)","Author":"Adams, Joel"},{"Title":"Java: a beginners guide (2014)","Author":"Schildt, Herbert"},{"Title":"Java: the ultimate beginners guide (2016)","Author":"Johansen, Andrew"},{"Title":"Java 8 in action: lambdas, streams, and functional-style programming (2014)","Author":"Urma, Raoul-Gabriel; Fusco, Mario; Mycroft, Alan"}]';
//The variable is then parsed
var dataObject = JSON.parse(dataJSON);
//Creating a list item
var listItemString = $('#m6listItem').html();
//A new list is built for every item of the list (book)
dataObject.forEach(buildNewList);
//The list is then built
function buildNewList(item, index) {
  //The list item is stored between tags
  var listItem = $('<li>' + listItemString + '</li>');
  //The id where the list items are stored is identified
  var listItemTitle = $('.m6booktitle', listItem);
  //As well as the item in the list that should be stored within that id area
  listItemTitle.html(item.Title);
  //This is repeated for the author
  var listItemAuth = $('.m6author', listItem);
  listItemAuth.html(item.Author);
  $('#m6dataList').append(listItem);
}
