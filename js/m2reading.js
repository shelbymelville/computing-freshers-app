//The reading lists is stored within a variable
var dataJSON = '[{"Title":"Systems analysis and design (2014)","Author":"Kendall, Kenneth E.; Kendall, Julie E."},{"Title":"New Perspectives Computer Concepts 2018: Introductory (2017)","Author":"Parsons, June Jamrich"}, {"Title":"Systems analysis and design (2014)","Author":"Dennis, Alan"},{"Title":"Business analysis (2014)","Author":"Paul, Debra; Cadle, James; Yeates, Donald"}, {"Title":"Database principles and design (2008)","Author":"Ritchie, Colin; Ritchie, Colin"},{"Title":"Systems analysis and design in a changing world (2015)","Author":"Satzinger, John W.; Jackson, Robert B.; Burd, Stephen D."}]';
//The variable is then parsed
var dataObject = JSON.parse(dataJSON);
//Creating a list item
var listItemString = $('#m2listItem').html();
//A new list is built for every item of the list (book)
dataObject.forEach(buildNewList);
//The list is then built
function buildNewList(item, index) {
  //The list item is stored between tags
  var listItem = $('<li>' + listItemString + '</li>');
  //The id where the list items are stored is identified
  var listItemTitle = $('.m2booktitle', listItem);
  //As well as the item in the list that should be stored within that id area
  listItemTitle.html(item.Title);
  //This is repeated for the author
  var listItemAuth = $('.m2author', listItem);
  listItemAuth.html(item.Author);
  $('#m2dataList').append(listItem);
}
