//The existing questions are stored in a variable
var jcontent = {
    "supportq": "Who can I go to if I have an issue whilst living on campus?",
    "supporta": "The Campus Life team, you can call them on 01695 657570 or email campuslife@edgehill.ac.uk",

    "facilitiesq": "What time does McColls shut?",
    "facilitiesa": "The opening hours are 6AM-11PM"
}
//The variable is to be outputted into the element in the HTML with the corresponding id
var output = document.getElementById('support-question');
//The text inputted into the HTML identified with the id is then set, using the identifiers used to store the information in the variable
output.innerHTML = "<strong>Q:</strong>" + " " + jcontent.supportq;
//This is then repeated for the other links using the same method
var output = document.getElementById('support-answer');
output.innerHTML = "<strong>A:</strong>" + " " + jcontent.supporta;

var output = document.getElementById('facilities-question');
output.innerHTML = "<strong>Q:</strong>" + " " + jcontent.facilitiesq;

var output = document.getElementById('facilities-answer');
output.innerHTML = "<strong>A:</strong>" + " " +jcontent.facilitiesa;
